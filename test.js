const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let arr = [];

function askQuestion() {
  return new Promise((resolve) => {
    rl.question('Enter a value (or type "done" to exit): ', (answer) => {
      resolve(answer);
    });
  });
}

async function readData() {
  while (true) {
    const value = await askQuestion();
    if (value === "done") {
      break;
    }
    arr.push(value);
  }
  console.log("Data entered:", arr);
  rl.close();
}

readData();
